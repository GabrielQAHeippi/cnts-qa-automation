import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC, select
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.action_chains import ActionChains
from Functions.Funtions import General_functions



class Autentication(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome(executable_path='C:\Dchroom\R\chromedriver.exe')

    def test_Consignacion(self):
        driver = self.driver
        driver.get('https://ejerzo.dev.consejonacionaldetrabajosocial.org.co/authentication/login')
        driver.maximize_window()
        driver.find_element(By.XPATH, '//*[@id="mat-input-0"]').send_keys('qa502@yopmail.com')
        driver.find_element(By.XPATH,'//*[@id="mat-input-1"]').send_keys('gabriel123.')
        driver.find_element(By.XPATH, '//*[@id="login-form"]/form/button').click()

        try:

            WebDriverWait(self.driver, 5).until(EC.visibility_of_element_located((By.XPATH, '//*[@id="boards"]/div[2]/button[1]')))
            driver.find_element(By.XPATH, '//*[@id="boards"]/div[2]/button[1]').click()
            time.sleep(5)
            driver.find_element(By.XPATH, '//*[@id="course-content"]/div/div[1]').click()
            driver.find_element(By.XPATH, '//*[@id="main-stepper"]/div[2]').click()
            driver.find_element(By.XPATH, '/html/body/app/app-layoutfor-professional/div/div/div/div/content/app-application-for/div/div/div[2]/div/div[1]/mat-horizontal-stepper/div[2]/div[1]').click()
            driver.find_element(By.XPATH, '/html/body/app/app-layoutfor-professional/div/div/div/div/content/app-application-for/div/div/div[2]/div/div[1]/mat-horizontal-stepper/div[2]/div[1]/div[3]/button[2]').click()
            #driver.execute_script("arguments[0].scrollIntoView();", '/html/body/app/app-layoutfor-professional/div/div/div/div/content/app-application-for/div/div/div[2]/div/div[1]/mat-horizontal-stepper/div[2]/div[1]/div[3]/button[2]')
            driver.find_element(By.XPATH, '/html/body/app/app-layoutfor-professional/div/div/div/div/content/app-application-for/div/div/div[2]/div/div[1]/mat-horizontal-stepper/div[2]/div[2]/form').click()
            driver.find_element(By.XPATH, '/html/body/app/app-layoutfor-professional/div/div/div/div/content/app-application-for/div/div/div[2]/div/div[1]/mat-horizontal-stepper/div[2]/div[2]/form/div[2]/div[2]/mat-checkbox/label/div').click()
            driver.find_element(By.XPATH, '/html/body/app/app-layoutfor-professional/div/div/div/div/content/app-application-for/div/div/div[2]/div/div[1]/mat-horizontal-stepper/div[2]/div[2]/form/div[2]/mat-checkbox/label/div').click()
            driver.find_element(By.XPATH, '/html/body/app/app-layoutfor-professional/div/div/div/div/content/app-application-for/div/div/div[2]/div/div[1]/mat-horizontal-stepper/div[2]/div[2]/form/div[3]/button[2]').click()
            time.sleep(5)
            val = driver.find_element(By.XPATH, '/html/body/app/app-layoutfor-professional/div/div/div/div/content/app-application-for/div/div/div[2]/div/div[1]/mat-horizontal-stepper/div[2]/div[3]/form/div[4]/p')
            driver.execute_script("arguments[0].scrollIntoView();", val)
            driver.find_element(By.XPATH, '/html/body/app/app-layoutfor-professional/div/div/div/div/content/app-application-for/div/div/div[2]/div/div[1]/mat-horizontal-stepper/div[2]/div[3]/form/div[5]/button[2]').click()
            time.sleep(3)
            driver.find_element(By.XPATH, '/html/body/app/app-layoutfor-professional/div/div/div/div/content/app-application-for/div/div/div[2]/div/div[1]/mat-horizontal-stepper/div[2]/div[4]/form/div[3]/button[2]').click()
            time.sleep(3)
            val2 = driver.find_element(By.XPATH, '/html/body/app/app-layoutfor-professional/div/div/div/div/content/app-application-for/div/div/div[2]/div/div[1]/mat-horizontal-stepper/div[2]/div[5]/form/div[2]/div[1]/mat-form-field/div/div[1]')
            driver.execute_script("arguments[0].scrollIntoView();", val2)
            driver.find_element(By.XPATH, '/html/body/app/app-layoutfor-professional/div/div/div/div/content/app-application-for/div/div/div[2]/div/div[1]/mat-horizontal-stepper/div[2]/div[5]/form/div[2]/div[1]/mat-form-field/div/div[1]').click()
            driver.find_element(By.XPATH, '/html/body/div[1]/div[2]/div/div/div/mat-option[1]').click()
            driver.find_element(By.XPATH, '/html/body/app/app-layoutfor-professional/div/div/div/div/content/app-application-for/div/div/div[2]/div/div[1]/mat-horizontal-stepper/div[2]/div[5]/form/div[3]/button[2]').click()
            time.sleep(2)
            driver.find_element(By.XPATH, '/html/body/app/app-layoutfor-professional/div/div/div/div/content/app-application-for/div/div/div[2]/div/div[1]/mat-horizontal-stepper/div[2]/div[6]/div[2]/div/button[2]').click()
            time.sleep(2)
            val3 = driver.find_element(By.XPATH,'/html/body/app/app-layoutfor-professional/div/div/div/div/content/app-application-for/div/div/div[2]/div/div[1]/mat-horizontal-stepper/div[2]/div[7]/div[4]/button[2]')
            driver.execute_script("arguments[0].scrollIntoView();", val3)
            driver.find_element(By.XPATH, '/html/body/app/app-layoutfor-professional/div/div/div/div/content/app-application-for/div/div/div[2]/div/div[1]/mat-horizontal-stepper/div[2]/div[7]/div[4]/button[2]').click()
            #If is exist, keep going
            if driver.find_element(By.XPATH, '//*[@id="mat-radio-8"]/label/div[2]/span[2]'):
                print('Exist')

            '''if (Consig == 'Consignación'):
                print("Existe")
            else:
                print("No existe")'''
            time.sleep(10)


        except TimeoutException as ex:
            print(ex.msg)
            print("No existe")

    def tearDown(self):
        self.driver.close()

if __name__ == '__main__':
  unittest.main()