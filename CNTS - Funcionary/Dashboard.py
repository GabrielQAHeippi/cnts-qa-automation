import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions as EC, select
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.wait import WebDriverWait


class Dashboard(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Chrome(executable_path='C:\Dchroom\R\chromedriver.exe')

    def test_displays(self):
        driver = self.driver
        driver.get("https://www.google.com")
        driver.implicitly_wait(5)
        displaelements = driver.find_element(By.NAME,'btnK')
        print(displaelements.is_displayed())
        print(displaelements.is_enabled())


    def test_exist(self):
        driver = self.driver
        driver.get("https://www.google.com")

        try:
            if driver.find_element(By.NAME, 'btnK'):
                print("Existe")
            else:
                print("No existe")

        except TimeoutException as ex:
            print(ex.msg)

    def tearDown(self):
        self.driver.close()


if __name__ == "__main__":
    unittest.main()